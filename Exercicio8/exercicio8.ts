/*
Faça um programa que receba o valor de um depósito e o
valor da taxa de juros, calcule e mostre o valor do rendimento
e o valor total depois do rendimento.
*/

//Inicio

namespace exercicio8 
{
    let deposito: number;
    let juros: number;

    deposito = 1000;
    juros = deposito * 20/100 ;

    let rendimento: number;

    rendimento = deposito + juros;

    console.log(`O valor do rendimento foi= ${rendimento}`);

}