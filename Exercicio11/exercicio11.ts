/*
Faça um programa que receba um número positivo e
maior que zero, calcule e mostre:
a) O número digitado ao quadrado;
b) O número digitado ao cubo;
c) A raiz quadrada do número digitado;
d) A raiz cúbica de número digitado.
*/

//Inicio
namespace exercicio11 
{
    //Entrada de dados
    const numero =10;

    let numQ: number;
    
    numQ = numero * numero;

    let numC: number;
    
    numC = Math.pow (numero, 3);

    let raizQ: number;

    raizQ = Math.sqrt (numero);

    let raizC: number;

    raizC = Math.cbrt (numero);

    console.log(`O numero elevado ao quadrado: ${numQ} \n o numeor elevado ao cubo: ${numC} \n A raiz quadrado do numero: ${raizQ} \n A raiz cubica do numero: ${raizC}`)

}