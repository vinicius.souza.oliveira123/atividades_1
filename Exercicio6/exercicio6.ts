/*
Faça um programa que receba o salário-base de um
funcionário, calcule e mostre o salário a receber, sabendo-se
que esse funcionário tem gratificação de 5% sobre o salário
base e paga imposto de 7% sobre o salário-base.
*/

//Inicio

namespace exercicio6
{
    let salario_base: number;
    let aumento: number;
    let imposto: number;

    salario_base = 1000;
    aumento = 0.05;
    imposto= 0.07;

    let novo_salario: number;

    

    novo_salario = salario_base + (salario_base * aumento) -  (salario_base * imposto);

    console.log(`O salario a receber será= ${novo_salario}`);

}