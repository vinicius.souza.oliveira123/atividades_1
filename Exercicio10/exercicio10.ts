/*
Faça um programa que calcule e mostre a área de um
círculo. Sabe-se que: Área = π.R2
*/

//Inicio

namespace exercicio10 
{
    let pi: number;
    let raio: number;
    let area: number;

    pi = 3.14;
    raio = 3;
    
    area = pi * (raio * raio);

    console.log(`A area do circulo é= ${area}`);


}