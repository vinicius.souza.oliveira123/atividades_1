/*
Faça um programa que receba três notas, calcule e mostre a
média ponderada entre elas.
*/

//Inicio

namespace exercicio3
{
    let nota1, nota2, nota3: number;

    let peso1, peso2, peso3: number;

    nota1 = 7;
    nota2 = 2;
    nota3 = 5;

    peso1 = 3;
    peso2 = 1;
    peso3 = 6;

    let resultado: number;

    resultado = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);

    console.log("O resultado da media ponderada é = " + resultado);

} 