/*
Faça um programa que receba o salário-base de um
funcionário, calcule e mostre o seu salário a receber, sabendo-
se que esse funcionário tem gratificação de R$50,00 e paga
imposto de 10% sobre o salário-base.
*/

//Inicio

namespace exercicio7 
{
    let salario_base: number;
    let gratificação: number;
    let imposto: number;

    salario_base = 1000
    gratificação = 50
    imposto = 10/100

    let novo_salario: number

    novo_salario = (salario_base + gratificação) - (salario_base * imposto);

    console.log(`O salario a receber será: ${novo_salario}`);


}